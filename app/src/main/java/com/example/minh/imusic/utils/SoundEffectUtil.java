package com.example.minh.imusic.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.audiofx.Equalizer;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.activity.MainActivity;

/**
 * Created by Minh on 5/10/2016.
 */
public class SoundEffectUtil {
    // volume
    public static void setVolume(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        //(LAYOUT)
        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogLayout = inflater.inflate(R.layout.activity_main_volume, null);
        final SeekBar seekBarVolume = (SeekBar) dialogLayout.findViewById(R.id.seek_bar_volume);
        RelativeLayout relativeMute = (RelativeLayout) dialogLayout.findViewById(R.id.mute);
        final ImageView imageMute = (ImageView) dialogLayout.findViewById(R.id.image_mute);
        final AudioManager audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        seekBarVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        int checkVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (checkVolume == 0) {
            imageMute.setImageResource(R.drawable.ic_volume_mute);
        } else {
            imageMute.setImageResource(R.drawable.ic_volume);
        }
        seekBarVolume.setProgress(checkVolume);

        seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar arg0)
            {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0)
            {
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
            {
                if (progress == 0) {
                    imageMute.setImageResource(R.drawable.ic_volume_mute);
                } else {
                    imageMute.setImageResource(R.drawable.ic_volume);
                }
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }
        });

        relativeMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageMute.setImageResource(R.drawable.ic_volume_mute);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                seekBarVolume.setProgress(0);
            }
        });

        //(DIALOG)
        dialog.setView(dialogLayout);
        dialog.show();
    }

    // equalizer
    @SuppressLint("SetTextI18n")
    public static void setEqualizer(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Equalizer equalizer = new Equalizer(0, MainActivity.mediaPlayer.getAudioSessionId());
        equalizer.setEnabled(true);

        final short minEQLevel = equalizer.getBandLevelRange()[0];
            short maxEQLevel = equalizer.getBandLevelRange()[1];

        //(LAYOUT)
        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogLayout = inflater.inflate(R.layout.activity_main_equalizer, null);
        TextView max = (TextView) dialogLayout.findViewById(R.id.max);
        TextView min = (TextView) dialogLayout.findViewById(R.id.min);
        max.setText("+" + (maxEQLevel / 100) + " dB");
        min.setText((minEQLevel / 100) + " dB");

        SeekBar bass = (SeekBar) dialogLayout.findViewById(R.id.mySeekBar);
        bass.setMax(maxEQLevel - minEQLevel);
        bass.setProgress((maxEQLevel - minEQLevel) / 2);
        bass.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                equalizer.setBandLevel((short) 0, (short) (progress + minEQLevel));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SeekBar low = (SeekBar) dialogLayout.findViewById(R.id.mySeekBar1);
        low.setMax(maxEQLevel - minEQLevel);
        low.setProgress((maxEQLevel - minEQLevel) / 2);
        low.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                equalizer.setBandLevel((short) 1, (short) (progress + minEQLevel));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SeekBar mid = (SeekBar) dialogLayout.findViewById(R.id.mySeekBar2);
        mid.setMax(maxEQLevel - minEQLevel);
        mid.setProgress((maxEQLevel - minEQLevel) / 2);
        mid.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                equalizer.setBandLevel((short) 2, (short) (progress + minEQLevel));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SeekBar upper = (SeekBar) dialogLayout.findViewById(R.id.mySeekBar3);
        upper.setMax(maxEQLevel - minEQLevel);
        upper.setProgress((maxEQLevel - minEQLevel) / 2);
        upper.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                equalizer.setBandLevel((short) 3, (short) (progress + minEQLevel));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SeekBar high = (SeekBar) dialogLayout.findViewById(R.id.mySeekBar4);
        high.setMax(maxEQLevel - minEQLevel);
        high.setProgress((maxEQLevel - minEQLevel) / 2);
        high.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                equalizer.setBandLevel((short) 4, (short) (progress + minEQLevel));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //(DIALOG)
        dialog.setView(dialogLayout);
        dialog.show();
    }
}
