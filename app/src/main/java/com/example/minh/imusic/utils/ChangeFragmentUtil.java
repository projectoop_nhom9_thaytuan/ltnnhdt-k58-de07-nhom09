package com.example.minh.imusic.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.minh.imusic.R;

/**
 * Created by Minh on 5/6/2016.
 */
public class ChangeFragmentUtil {
    public static void changeFragment(Fragment targetFragment, FragmentManager fragmentManager){
        fragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, targetFragment)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }
}
