package com.example.minh.imusic.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.minh.imusic.R;
import com.example.minh.imusic.utils.ChangeFragmentUtil;
import com.example.minh.imusic.utils.SoundEffectUtil;

/**
 * Created by Minh on 5/6/2016.
 */
public class MainFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout mRelativeSongs;
    private RelativeLayout mRelativeFolders;
    private RelativeLayout mRelativeVolume;
    private RelativeLayout mRelativeFavorites;

    public static int checkFavorite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mRelativeSongs = (RelativeLayout) view.findViewById(R.id.action_songs);
        mRelativeFolders = (RelativeLayout) view.findViewById(R.id.action_folders);
        mRelativeVolume = (RelativeLayout) view.findViewById(R.id.action_volume);
        mRelativeFavorites = (RelativeLayout) view.findViewById(R.id.action_favorites);
        mRelativeSongs.setOnClickListener(this);
        mRelativeFolders.setOnClickListener(this);
        mRelativeVolume.setOnClickListener(this);
        mRelativeFavorites.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_songs:
                checkFavorite = 0;
                ChangeFragmentUtil.changeFragment(new SongsFragment(), getActivity().getSupportFragmentManager());
                break;
            case R.id.action_folders:
                checkFavorite = 0;
                ChangeFragmentUtil.changeFragment(new FoldersFragment(), getActivity().getSupportFragmentManager());
                break;
            case R.id.action_volume:
                SoundEffectUtil.setVolume(getActivity());
                break;
            case R.id.action_favorites:
                checkFavorite = 1;
                ChangeFragmentUtil.changeFragment(new FavoritesFragment(), getActivity().getSupportFragmentManager());
                break;
        }
    }
}
