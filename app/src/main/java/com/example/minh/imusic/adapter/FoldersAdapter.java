package com.example.minh.imusic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.object.Folders;

import java.util.ArrayList;

/**
 * Created by Minh on 5/7/2016.
 */
public class FoldersAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Folders> folders;
    private Folders info;

    class MyViewHolder {
        private TextView mTextFolderName;
        private TextView mTextFolderSize;

        MyViewHolder (View view) {
            mTextFolderName = (TextView) view.findViewById(R.id.text_folder_name);
            mTextFolderSize = (TextView) view.findViewById(R.id.text_folder_size);
        }
    }

    public FoldersAdapter (Context context, ArrayList<Folders> folders) {
        this.context = context;
        this.folders = folders;
    }

    @Override
    public int getCount() {
        return folders.size();
    }

    @Override
    public Object getItem(int position) {
        return folders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    public View getView (final int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_folders_row , null);
            holder = new MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }

        info = folders.get(position);
        holder.mTextFolderName.setText(info.getmName());
        holder.mTextFolderSize.setText(info.getmQuantity() + " item(s) | " + info.getmDateCreated());

        return convertView;
    }

}
