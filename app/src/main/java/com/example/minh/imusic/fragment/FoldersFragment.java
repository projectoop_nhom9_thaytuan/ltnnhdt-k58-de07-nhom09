package com.example.minh.imusic.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.activity.MainActivity;
import com.example.minh.imusic.adapter.FoldersAdapter;
import com.example.minh.imusic.object.Folders;
import com.example.minh.imusic.utils.ChangeFragmentUtil;
import com.example.minh.imusic.utils.MusicUtil;

import java.util.ArrayList;

/**
 * Created by Minh on 5/7/2016.
 */
public class FoldersFragment extends Fragment {
    private ListView list;
    private FoldersAdapter adapter;

    private ArrayList<String> folder;
    private ArrayList<Folders> folders;

    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_folders, container, false);

        folder = MusicUtil.getMusicFolderPath(getActivity());
        folders = MusicUtil.getFoldersMusic(folder);
        list = (ListView) view.findViewById(R.id.list);
        adapter = new FoldersAdapter(getActivity(), folders);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SongsOfFolderFragment songsOfFolderFragment = new SongsOfFolderFragment();
                bundle = new Bundle();
                bundle.putString("pathFolder", folders.get(position).getmPath());
                songsOfFolderFragment.setArguments(bundle);
                ChangeFragmentUtil.changeFragment(songsOfFolderFragment, getActivity().getSupportFragmentManager());
            }
        });
        if (MainActivity.IS_MAIN_FRAG_STATES == 0)
            MainActivity.IS_MAIN_FRAG_STATES = 1;
        else MainActivity.IS_MAIN_FRAG_STATES = 2;
        return view;
    }
}
