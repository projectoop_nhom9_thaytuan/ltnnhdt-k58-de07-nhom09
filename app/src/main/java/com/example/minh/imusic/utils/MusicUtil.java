package com.example.minh.imusic.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.activity.MainActivity;
import com.example.minh.imusic.adapter.SongsAdapter;
import com.example.minh.imusic.object.Folders;
import com.example.minh.imusic.object.Songs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by Minh on 5/7/2016.
 */
public class MusicUtil {
    // get list songs
    public static ArrayList<Songs> getAllSongs(Activity activity) {
        ArrayList<Songs> songs = new ArrayList<>();
        String sortOrder = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";
        Songs info;
        String selection = "_data like ?";
        String[] selections = new String[]{"%.mp3%"};
        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM_ID,
        };
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = activity.getContentResolver().query(uri, projection, selection, selections, sortOrder);
        if (musicCursor != null) {
            while (musicCursor.moveToNext()) {
                if (ConvertUtil.cutStringAfter(musicCursor.getString(0)).compareTo("/storage/emulated/0/Musics/OOPFavoriteSongs") != 0) {
                    info = new Songs();
                    info.setmName(ConvertUtil.cutStringBefore(musicCursor.getString(0)).replace(".mp3", ""));
                    info.setmPath(musicCursor.getString(0));
                    info.setmDuration(musicCursor.getLong(1));
                    info.setmArtist(musicCursor.getString(2));
                    info.setmThumbnailID(musicCursor.getLong(3));
                    songs.add(info);
                }
            }
        }
        if (musicCursor != null) {
            musicCursor.close();
        }
        Collections.sort(songs, new SongsComparator());

        return songs;
    }

    private static class SongsComparator implements Comparator<Songs> {
        public int compare(Songs left, Songs right) {
            return left.getmName().compareTo(right.getmName());
        }
    }

    // get list folders
    public static ArrayList<Folders> getFoldersMusic(ArrayList<String> folder) {
        ArrayList folders = new ArrayList();
        Folders info;
        for (int i = 0; i < folder.size(); i++) {
            info = new Folders();
            info.setmName(ConvertUtil.cutStringBefore(folder.get(i)));
            info.setmPath(folder.get(i));
            info.setmQuantity((new File(info.getmPath())).listFiles().length);
            info.setmDateCreated(getFolderMusicDateCreated(new File(info.getmPath())));
            folders.add(info);
        }
        Collections.sort(folders, new FolderNameComparator());

        return folders;
    }

    private static String getFolderMusicDateCreated(File file) {
        String dateFormat = "dd/MM/yyyy hh:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        return simpleDateFormat.format(file.lastModified());
    }

    private static class FolderNameComparator implements Comparator<Folders> {
        public int compare(Folders left, Folders right) {
            return left.getmName().compareTo(right.getmName());
        }
    }

    // music folder path
    public static ArrayList<String> getMusicFolderPath(Activity activity) {
        ArrayList<String> folder = new ArrayList<>();
        String selection = "_data like ?";
        String[] selections = new String[]{"%.mp3%"};
        String[] projection = {
                MediaStore.Audio.Media.DATA,
        };
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = activity.getContentResolver().query(uri, projection, selection, selections, null);
        if (musicCursor != null) {
            while (musicCursor.moveToNext()) {
                if (ConvertUtil.cutStringAfter(musicCursor.getString(0)).compareTo("/storage/emulated/0/Musics/OOPFavoriteSongs") != 0) {
                    folder.add(ConvertUtil.cutStringAfter(musicCursor.getString(0)));
                }
            }
        }
        if (musicCursor != null) {
            musicCursor.close();
        }

        HashSet set = new HashSet(folder);
        folder.clear();
        folder.addAll(set);

        return folder;
    }

    // list music of folder
    public static ArrayList<Songs> getMusicOfFolder(Activity activity, String folderPath) {
        ArrayList<Songs> songs = new ArrayList<>();
        String sortOrder = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";
        Songs info;
        String selection = "_data like ?";
        String[] selections = new String[]{"%.mp3%"};
        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM_ID,
        };
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = activity.getContentResolver().query(uri, projection, selection, selections, sortOrder);
        if (musicCursor != null) {
            while (musicCursor.moveToNext()) {
                if (ConvertUtil.cutStringAfter(musicCursor.getString(0)).compareTo(folderPath) == 0) {
                    info = new Songs();
                    info.setmName(ConvertUtil.cutStringBefore(musicCursor.getString(0)).replace(".mp3", ""));
                    info.setmPath(musicCursor.getString(0));
                    info.setmDuration(musicCursor.getLong(1));
                    info.setmArtist(musicCursor.getString(2));
                    info.setmThumbnailID(musicCursor.getLong(3));
                    songs.add(info);
                }
            }
        }
        if (musicCursor != null) {
            musicCursor.close();
        }

        return songs;
    }

    // add favorite
    public static void addFavorite(String path, String name) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        OutputStream outputStream = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/Musics/OOPFavoriteSongs/" + name + ".mp3");

        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    public static ArrayList<Songs> getFavoriteSongs(Activity activity) {
        ArrayList<Songs> songs = new ArrayList<>();
        Songs info;
        String sortOrder = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";
        String selection = "_data like ?";
        String[] selections = new String[]{"%.mp3%"};
        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM_ID,
        };
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        @SuppressLint("Recycle") Cursor musicCursor = activity.getContentResolver().query(uri, projection, selection, selections, sortOrder);
        if (musicCursor != null) {
            while (musicCursor.moveToNext()) {
                if (ConvertUtil.cutStringAfter(musicCursor.getString(0)).compareTo("/storage/emulated/0/Musics/OOPFavoriteSongs") == 0) {
                    info = new Songs();
                    info.setmName(ConvertUtil.cutStringBefore(musicCursor.getString(0)).replace(".mp3", ""));
                    info.setmPath(musicCursor.getString(0));
                    info.setmDuration(musicCursor.getLong(1));
                    info.setmArtist(musicCursor.getString(2));
                    info.setmThumbnailID(musicCursor.getLong(3));
                    songs.add(info);
                }
            }
        }
        if (musicCursor != null) {
            musicCursor.close();
        }

        return songs;
    }

    // action shuffle
    public static void setShuffle(int checkShuffle, ImageView...image) {
        resetImage(image);
        switch (checkShuffle) {
            case 1:
                image[0].setImageResource(R.drawable.ic_shuffle_highlight);
                break;
            case 2:
                image[1].setImageResource(R.drawable.ic_repeat_all_highlight);
                break;
            case 3:
                image[2].setImageResource(R.drawable.ic_repeat_one_highlight);
                break;
        }
    }
    private static void resetImage(ImageView... image) {
        image[0].setImageResource(R.drawable.ic_shuffle);
        image[1].setImageResource(R.drawable.ic_repeat_all);
        image[2].setImageResource(R.drawable.ic_repeat_one);
    }

    // auto next
    public static void autoNext(final Context context, final TextView... text) {
        MainActivity.mediaPlayer = MediaPlayer.create(context, Uri.parse(SongsAdapter.songs.get(SongsAdapter.current_position).getmPath()));
        MainActivity.mediaPlayer.start();
        text[1].setText(SongsAdapter.songs.get(SongsAdapter.current_position).getmName());
        text[2].setText(SongsAdapter.songs.get(SongsAdapter.current_position).getmArtist());
        text[0].setText(ConvertUtil.convertDuration(SongsAdapter.songs.get(SongsAdapter.current_position).getmDuration()));
        MainActivity.seek_bar.setMax(MainActivity.mediaPlayer.getDuration());
        MainActivity.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                int checkShuffleMusic = sharedPreferences.getInt(Values.ACTION_SHUFFLE, 0);
                if (checkShuffleMusic == 1) {
                    int temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                    while (temp == SongsAdapter.current_position) {
                        temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                    }
                    SongsAdapter.current_position = temp;
                } else if (checkShuffleMusic == 2) {
                    if (SongsAdapter.current_position == SongsAdapter.songs.size() - 1) {
                        SongsAdapter.current_position = 0;
                    } else {
                        SongsAdapter.current_position++;
                    }
                }
                autoNext(context, text[0], text[1], text[2]);
            }
        });
    }
}
