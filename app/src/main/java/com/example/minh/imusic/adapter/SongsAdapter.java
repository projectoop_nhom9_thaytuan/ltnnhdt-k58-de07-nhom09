package com.example.minh.imusic.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minh.imusic.R;
import com.example.minh.imusic.activity.MainActivity;
import com.example.minh.imusic.object.Songs;
import com.example.minh.imusic.utils.ConvertUtil;
import com.example.minh.imusic.utils.MusicUtil;
import com.example.minh.imusic.utils.Values;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Minh on 5/7/2016.
 */
public class SongsAdapter extends BaseAdapter {
    private Context context;
    private Activity activity;
    public static ArrayList<Songs> songs;
    private Songs info;
    public static int current_position = 0;

    private TextView mTextSongName;
    private TextView mTextSongArtist;
    private TextView mTextStartDuration;
    private TextView mTextTotalDuration;
    private RelativeLayout mImageFavorite;
    private ImageView mImageCD;
    private ImageView mImageActionPlay;
    private ImageView mImageFirst;
    private ImageView mImageLast;
    // check shuffle
    private SharedPreferences sharedPreferences;
    private int checkShuffleMusic;

    class MyViewHolder {
        private TextView textMusicName;
        private TextView textMusicDuration;
        private TextView textMusicArtist;
        private RelativeLayout row_layout;

        MyViewHolder(View view) {
            textMusicName = (TextView) view.findViewById(R.id.text_music_name);
            textMusicDuration = (TextView) view.findViewById(R.id.text_music_duration);
            textMusicArtist = (TextView) view.findViewById(R.id.text_music_artist);
            row_layout = (RelativeLayout) view.findViewById(R.id.row_layout);
        }
    }

    public SongsAdapter(Context context, Activity activity, ArrayList<Songs> songs, TextView songName, TextView songArtist,
                        TextView textStartDuration, TextView textTotalDuration, RelativeLayout relativeFavorite, ImageView... image) {
        this.context = context;
        this.activity = activity;
        this.songs = songs;

        this.mTextSongName = songName;
        this.mTextSongArtist = songArtist;
        this.mTextStartDuration = textStartDuration;
        this.mTextTotalDuration = textTotalDuration;
        this.mImageFavorite = relativeFavorite;
        this.mImageActionPlay = image[0];
        this.mImageCD = image[1];
        this.mImageFirst = image[2];
        this.mImageLast = image[3];
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_songs_row, null);
            holder = new MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }

        info = songs.get(position);
        holder.textMusicName.setText(info.getmName());
        holder.textMusicDuration.setText(ConvertUtil.convertDuration(info.getmDuration()));
        holder.textMusicArtist.setText(info.getmArtist());

        holder.row_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                checkShuffleMusic = sharedPreferences.getInt(Values.ACTION_SHUFFLE, 0);
                // set toolbar sliding up panel
                mImageActionPlay.setImageResource(R.drawable.ic_pause);

                current_position = position;
                if (MainActivity.mediaPlayer.isPlaying()) {
                    MainActivity.mediaPlayer.stop();
                    MainActivity.mediaPlayer.reset();
                    MainActivity.mHandler.removeMessages(0);
                }
                MusicUtil.autoNext(context, mTextTotalDuration, mTextSongName, mTextSongArtist);
                mImageCD.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotates360));

                MainActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                mTextStartDuration.setText(ConvertUtil.convertDuration(0));
                MainActivity.seek_bar.setProgress(0);
                MainActivity.mHandler = new Handler();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(MainActivity.mediaPlayer != null){
                            int mCurrentPosition = MainActivity.mediaPlayer.getCurrentPosition();
                            MainActivity.seek_bar.setProgress(mCurrentPosition);
                            mTextStartDuration.setText(ConvertUtil.convertDuration(MainActivity.mediaPlayer.getCurrentPosition()));
                        }
                        MainActivity.mHandler.postDelayed(this, 1);
                    }
                });

                MainActivity.action_pre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                        checkShuffleMusic = sharedPreferences.getInt(Values.ACTION_SHUFFLE, 0);
                        mImageActionPlay.setImageResource(R.drawable.ic_pause);
                        if (SongsAdapter.songs != null) {
                            if (checkShuffleMusic == 1) {
                                int temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                                while (temp == SongsAdapter.current_position) {
                                    temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                                }
                                SongsAdapter.current_position = temp;
                            } else if ((checkShuffleMusic == 2 || checkShuffleMusic == 3) && SongsAdapter.current_position == 0) {
                                SongsAdapter.current_position = SongsAdapter.songs.size() - 1;
                            } else if (SongsAdapter.current_position != 0) {
                                SongsAdapter.current_position -= 1;
                            }
                            if (MainActivity.mediaPlayer.isPlaying()) {
                                MainActivity.mediaPlayer.stop();
                                MainActivity.mediaPlayer.reset();
                                MainActivity.mHandler.removeMessages(0);
                            }
                            MusicUtil.autoNext(context, mTextTotalDuration, mTextSongName, mTextSongArtist);
                            MainActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                            mTextStartDuration.setText(ConvertUtil.convertDuration(0));
                            MainActivity.mHandler = new Handler();
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if(MainActivity.mediaPlayer != null){
                                        int mCurrentPosition = MainActivity.mediaPlayer.getCurrentPosition();
                                        MainActivity.seek_bar.setProgress(mCurrentPosition);
                                        mTextStartDuration.setText(ConvertUtil.convertDuration(MainActivity.mediaPlayer.getCurrentPosition()));
                                    }
                                    MainActivity.mHandler.postDelayed(this, 1);
                                }
                            });
                        }
                    }
                });

                MainActivity.action_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                        checkShuffleMusic = sharedPreferences.getInt(Values.ACTION_SHUFFLE, 0);
                        mImageActionPlay.setImageResource(R.drawable.ic_pause);
                        if (SongsAdapter.songs != null) {
                            if (checkShuffleMusic == 1) {
                                int temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                                while (temp == SongsAdapter.current_position) {
                                    temp = new Random().nextInt(SongsAdapter.songs.size() - 1);
                                }
                                SongsAdapter.current_position = temp;
                            } else if ((checkShuffleMusic == 2 || checkShuffleMusic == 3) && SongsAdapter.current_position == SongsAdapter.songs.size() - 1) {
                                SongsAdapter.current_position = 0;
                            } else if (SongsAdapter.current_position != SongsAdapter.songs.size() - 1) {
                                SongsAdapter.current_position += 1;
                            }
                            if (MainActivity.mediaPlayer.isPlaying()) {
                                MainActivity.mediaPlayer.stop();
                                MainActivity.mediaPlayer.reset();
                                MainActivity.mHandler.removeMessages(0);
                            }
                            MusicUtil.autoNext(context, mTextTotalDuration, mTextSongName, mTextSongArtist);
                            MainActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                            mTextStartDuration.setText(ConvertUtil.convertDuration(0));
                            MainActivity.mHandler = new Handler();
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if(MainActivity.mediaPlayer != null){
                                        int mCurrentPosition = MainActivity.mediaPlayer.getCurrentPosition();
                                        MainActivity.seek_bar.setProgress(mCurrentPosition);
                                        mTextStartDuration.setText(ConvertUtil.convertDuration(MainActivity.mediaPlayer.getCurrentPosition()));
                                    }
                                    MainActivity.mHandler.postDelayed(this, 1);
                                }
                            });
                        }
                    }
                });

                mImageLast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mImageActionPlay.setImageResource(R.drawable.ic_pause);
                        if (SongsAdapter.songs != null) {
                            SongsAdapter.current_position = SongsAdapter.songs.size() - 1;
                            if (MainActivity.mediaPlayer.isPlaying()) {
                                MainActivity.mediaPlayer.stop();
                                MainActivity.mediaPlayer.reset();
                                MainActivity.mHandler.removeMessages(0);
                            }
                            MusicUtil.autoNext(context, mTextTotalDuration, mTextSongName, mTextSongArtist);
                            MainActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                            mTextStartDuration.setText(ConvertUtil.convertDuration(0));
                            MainActivity.mHandler = new Handler();
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if(MainActivity.mediaPlayer != null){
                                        int mCurrentPosition = MainActivity.mediaPlayer.getCurrentPosition();
                                        MainActivity.seek_bar.setProgress(mCurrentPosition);
                                        mTextStartDuration.setText(ConvertUtil.convertDuration(MainActivity.mediaPlayer.getCurrentPosition()));
                                    }
                                    MainActivity.mHandler.postDelayed(this, 1);
                                }
                            });
                        }
                    }
                });

                mImageFirst.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mImageActionPlay.setImageResource(R.drawable.ic_pause);
                        if (SongsAdapter.songs != null) {
                            SongsAdapter.current_position = 0;
                            if (MainActivity.mediaPlayer.isPlaying()) {
                                MainActivity.mediaPlayer.stop();
                                MainActivity.mediaPlayer.reset();
                                MainActivity.mHandler.removeMessages(0);
                            }
                            MusicUtil.autoNext(context, mTextTotalDuration, mTextSongName, mTextSongArtist);
                            MainActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                            mTextStartDuration.setText(ConvertUtil.convertDuration(0));
                            MainActivity.mHandler = new Handler();
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if(MainActivity.mediaPlayer != null){
                                        int mCurrentPosition = MainActivity.mediaPlayer.getCurrentPosition();
                                        MainActivity.seek_bar.setProgress(mCurrentPosition);
                                        mTextStartDuration.setText(ConvertUtil.convertDuration(MainActivity.mediaPlayer.getCurrentPosition()));
                                    }
                                    MainActivity.mHandler.postDelayed(this, 1);
                                }
                            });
                        }
                    }
                });
            }
        });

        // add to favorite
        mImageFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File favorite = new File(Environment.getExternalStorageDirectory().getPath() + "/Musics/OOPFavoriteSongs/");
                if (!favorite.exists()) {
                    favorite.mkdirs();
                }
                try {
                    MusicUtil.addFavorite(songs.get(current_position).getmPath(), songs.get(current_position).getmName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(Environment.getExternalStorageDirectory().getPath() + "/Musics/OOPFavoriteSongs/" + songs.get(current_position).getmName() + ".mp3"))));
                Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
            }
        });

        LocalBroadcastManager.getInstance(context).registerReceiver(receiverPlayPause, new IntentFilter("PlayPause"));
        return convertView;
    }

    private BroadcastReceiver receiverPlayPause = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra("status");
            if (status.compareTo("pause") == 0) {
                mImageCD.clearAnimation();
            } else if (status.compareTo("play") == 0) {
                mImageCD.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotates360));
            }
        }
    };
}