package com.example.minh.imusic.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.fragment.MainFragment;
import com.example.minh.imusic.utils.ChangeFragmentUtil;
import com.example.minh.imusic.utils.ConvertUtil;
import com.example.minh.imusic.utils.MusicUtil;
import com.example.minh.imusic.utils.SoundEffectUtil;
import com.example.minh.imusic.utils.Values;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static SlidingUpPanelLayout mLayout;
    public static MediaPlayer mediaPlayer;
    public static ImageView action_pre;
    public static ImageView action_next;
    private RelativeLayout slide_up_toolbar;
    public static SeekBar seek_bar;
    public static Handler mHandler;

    // action shuffle repeat_all repeat_one
    private ImageView mImageActionShuffle;
    private ImageView mImageActionRepeatAll;
    private ImageView mImageActionRepeatOne;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int checkShuffleMusic;
    // action play volume
    private TextView mTextStartDuration;
    private ImageView mImageActionPlay;
    private RelativeLayout mImageVolume;
    private RelativeLayout mImageEqualizer;

    private RelativeLayout mImageVavorite;

    public static int IS_MAIN_FRAG_STATES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_main);

        // action shuffle repeat_all repeat_one
        mImageActionShuffle = (ImageView) findViewById(R.id.action_shuffle);
        mImageActionRepeatAll = (ImageView) findViewById(R.id.action_repeat_all);
        mImageActionRepeatOne = (ImageView) findViewById(R.id.action_repeat_one);
        mImageActionShuffle.setOnClickListener(this);
        mImageActionRepeatAll.setOnClickListener(this);
        mImageActionRepeatOne.setOnClickListener(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        checkShuffleMusic = sharedPreferences.getInt(Values.ACTION_SHUFFLE, 0);
        MusicUtil.setShuffle(checkShuffleMusic, mImageActionShuffle, mImageActionRepeatAll, mImageActionRepeatOne);
        // action play volume
        mTextStartDuration = (TextView) findViewById(R.id.startTime);
        mImageActionPlay = (ImageView) findViewById(R.id.action_play);
        mImageVolume = (RelativeLayout) findViewById(R.id.action_volume);
        mImageEqualizer = (RelativeLayout) findViewById(R.id.action_equalizer);
        mImageActionPlay.setOnClickListener(this);
        mImageVolume.setOnClickListener(this);
        mImageEqualizer.setOnClickListener(this);

        mImageVavorite = (RelativeLayout) findViewById(R.id.relative_favorite);

        mediaPlayer = new MediaPlayer();

        ChangeFragmentUtil.changeFragment(new MainFragment(), getSupportFragmentManager());
        IS_MAIN_FRAG_STATES = 0;

        action_pre = (ImageView) findViewById(R.id.action_pre);
        action_next = (ImageView) findViewById(R.id.action_next);
        slide_up_toolbar = (RelativeLayout) findViewById(R.id.slide_up_toolbar);
        seek_bar = (SeekBar) findViewById(R.id.seek_bar);

        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }
        });

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    mImageVavorite.setVisibility(View.GONE);
                } else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED && MainFragment.checkFavorite != 1) {
                    mImageVavorite.setVisibility(View.VISIBLE);
                }
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
        slide_up_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
            if (IS_MAIN_FRAG_STATES == 0){
                MainActivity.this.finish();
            }
            else if (IS_MAIN_FRAG_STATES == 2){
                IS_MAIN_FRAG_STATES = 1;
            }
            else {
                IS_MAIN_FRAG_STATES = 0;
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_play:
                Intent intentPlayPause = new Intent("PlayPause");
                if (mediaPlayer.isPlaying()) {
                    intentPlayPause.putExtra("status", "pause");
                    mediaPlayer.pause();
                    mHandler.removeMessages(0);
                    mImageActionPlay.setImageResource(R.drawable.ic_play);
                } else {
                    intentPlayPause.putExtra("status", "play");
                    mImageActionPlay.setImageResource(R.drawable.ic_pause);
                    mediaPlayer.start();
                    seek_bar.setMax(mediaPlayer.getDuration());
                    mHandler = new Handler();
                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (mediaPlayer != null) {
                                int mCurrentPosition = mediaPlayer.getCurrentPosition();
                                seek_bar.setProgress(mCurrentPosition);
                                mTextStartDuration.setText(ConvertUtil.convertDuration(mCurrentPosition));
                            }
                            mHandler.postDelayed(this, 1);
                        }
                    });
                }
                LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intentPlayPause);
                break;

            // sound effect
            case R.id.action_volume:
                SoundEffectUtil.setVolume(MainActivity.this);
                break;
            case R.id.action_equalizer:
                SoundEffectUtil.setEqualizer(MainActivity.this);
                break;

            // shuffle
            case R.id.action_shuffle:
                MusicUtil.setShuffle(1, mImageActionShuffle, mImageActionRepeatAll, mImageActionRepeatOne);
                editor.putInt(Values.ACTION_SHUFFLE, 1);
                editor.commit();
                break;
            case R.id.action_repeat_all:
                MusicUtil.setShuffle(2, mImageActionShuffle, mImageActionRepeatAll, mImageActionRepeatOne);
                editor.putInt(Values.ACTION_SHUFFLE, 2);
                editor.commit();
                break;
            case R.id.action_repeat_one:
                MusicUtil.setShuffle(3, mImageActionShuffle, mImageActionRepeatAll, mImageActionRepeatOne);
                editor.putInt(Values.ACTION_SHUFFLE, 3);
                editor.commit();
                break;
        }
    }
}
