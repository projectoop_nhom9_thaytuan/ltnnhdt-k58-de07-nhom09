package com.example.minh.imusic.object;

/**
 * Created by Minh on 5/7/2016.
 */
public class Folders {
    private String mName;
    private String mPath;
    private String mDateCreated;
    private int mQuantity;

    public String getmName() {
        return mName;
    }

    public String getmPath() {
        return mPath;
    }

    public String getmDateCreated() {
        return mDateCreated;
    }

    public int getmQuantity() {
        return mQuantity;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmPath(String mPath) {
        this.mPath = mPath;
    }

    public void setmDateCreated(String mDateCreated) {
        this.mDateCreated = mDateCreated;
    }

    public void setmQuantity(int mQuantity) {
        this.mQuantity = mQuantity;
    }
}
