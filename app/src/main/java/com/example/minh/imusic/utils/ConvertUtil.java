package com.example.minh.imusic.utils;

/**
 * Created by Minh on 5/7/2016.
 */
public class ConvertUtil {
    // duration
    public static String convertDuration (long mDuration) {
        long seconds;
        long minutes = 0;
        long hours = 0;
        seconds = mDuration / 1000;
        if(seconds >= 60) {
            minutes = seconds / 60;
            seconds %= 60;
            if(minutes > 60) {
                hours = minutes / 60;
                minutes %= 60;
            } else {
                if (seconds < 10) {
                    if (minutes < 10) {
                        return ("0" + minutes + ":0" + seconds);
                    } else {
                        return ("" + minutes + ":0" + seconds);
                    }
                } else if (seconds >= 10){
                    if (minutes < 10) {
                        return ("0" + minutes + ":" + seconds);
                    } else {
                        return ("" + minutes + ":" + seconds);
                    }
                }
            }
        } else {
            if (seconds < 10) {
                return ("0" + minutes + ":0" + seconds);
            } else if (seconds >= 10) {
                return ("0" + minutes + ":" + seconds);
            }
        }
        return ("" + hours + ":" + minutes + ":" + seconds);
    }

    // string
        // cut string after slash
    public static String cutStringAfter (String string) {
        for (int i = string.length() - 1 ; i >= 0 ; i--) {
            if ("/".compareTo(String.valueOf(string.charAt(i))) == 0) {
                return string.substring(0, i);
            }
        }
        return string;
    }
        // cut string before slash
    public static String cutStringBefore (String string) {
        for (int i = string.length() - 1 ; i >= 0 ; i--) {
            if ("/".compareTo(String.valueOf(string.charAt(i))) == 0) {
                return string.substring(i + 1);
            }
        }
        return string;
    }
}
