package com.example.minh.imusic.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.minh.imusic.R;
import com.example.minh.imusic.activity.MainActivity;
import com.example.minh.imusic.adapter.SongsAdapter;
import com.example.minh.imusic.object.Songs;
import com.example.minh.imusic.utils.MusicUtil;

import java.util.ArrayList;

/**
 * Created by Minh on 5/7/2016.
 */
public class SongsOfFolderFragment extends Fragment {
    private ListView list;
    private SongsAdapter adapter;

    private ArrayList<Songs> songs;

    private Bundle bundle;
    private String mPathFolders;

    private TextView mTextSongName;
    private TextView mTextSongArtist;
    private TextView mTextStartDuration;
    private TextView mTextTotalDuration;
    private RelativeLayout mImageFavorite;
    private ImageView mImageCD;
    private ImageView mImageActionPlay;
    private ImageView mImageFirst;
    private ImageView mImageLast;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_songs, container, false);

        mTextSongName = (TextView) getActivity().findViewById(R.id.song_name);
        mTextSongArtist = (TextView) getActivity().findViewById(R.id.song_artist);
        mTextStartDuration = (TextView) getActivity().findViewById(R.id.startTime);
        mTextTotalDuration = (TextView) getActivity().findViewById(R.id.totalTime);
        mImageFavorite = (RelativeLayout) getActivity().findViewById(R.id.relative_favorite);
        mImageCD = (ImageView) getActivity().findViewById(R.id.cd);
        mImageActionPlay = (ImageView) getActivity().findViewById(R.id.action_play);

        mImageFirst = (ImageView) getActivity().findViewById(R.id.action_first);
        mImageLast = (ImageView) getActivity().findViewById(R.id.action_last);

        bundle = this.getArguments();
        mPathFolders = bundle.getString("pathFolder");
        songs = MusicUtil.getMusicOfFolder(getActivity(), mPathFolders);

        list = (ListView) view.findViewById(R.id.list);
        adapter = new SongsAdapter(getActivity(),  getActivity(), songs, mTextSongName, mTextSongArtist, mTextStartDuration, mTextTotalDuration,
                mImageFavorite, mImageActionPlay, mImageCD, mImageFirst, mImageLast);
        list.setAdapter(adapter);
        MainActivity.IS_MAIN_FRAG_STATES = 2;
        return view;
    }
}
