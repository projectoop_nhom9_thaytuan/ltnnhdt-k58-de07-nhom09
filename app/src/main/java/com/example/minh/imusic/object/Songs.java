package com.example.minh.imusic.object;

/**
 * Created by Minh on 5/7/2016.
 */
public class Songs {
    private String mName;
    private String mPath;
    private String mArtist;
    private long mDuration;
    private long mThumbnailID;

    public String getmName() {
        return mName;
    }

    public String getmPath() {
        return mPath;
    }

    public String getmArtist() {
        return mArtist;
    }

    public long getmDuration() {
        return mDuration;
    }

    public long getmThumbnailID() {
        return mThumbnailID;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmPath(String mPath) {
        this.mPath = mPath;
    }

    public void setmArtist(String mArtist) {
        this.mArtist = mArtist;
    }

    public void setmDuration(long mDuration) {
        this.mDuration = mDuration;
    }

    public void setmThumbnailID(long mThumbnailID) {
        this.mThumbnailID = mThumbnailID;
    }
}
